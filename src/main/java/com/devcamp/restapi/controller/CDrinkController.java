package com.devcamp.restapi.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.model.CDrink;

@RestController
public class CDrinkController {
    @CrossOrigin
    @GetMapping("/devcamp-drinks")
    public ArrayList<CDrink> getDrink(){

        //Khai báo danh sách
        ArrayList<CDrink> listDrink = new ArrayList<>();
        LocalDate today = LocalDate.now(ZoneId.systemDefault());

        //Khởi tạo đối tượng
        CDrink tratac = new CDrink(1, "TRATAC", "Trà tắc", 10000, today, today);
        CDrink coca = new CDrink(2, "COCA", "Cocacala", 15000, today, today);
        CDrink pepsi = new CDrink(3, "PEPPSI", "Pepsi", 15000, today, today);
        
        //Thêm đối tượng vào danh sách
        listDrink.add(tratac);
        listDrink.add(coca);
        listDrink.add(pepsi);

        return listDrink;
    }
}
